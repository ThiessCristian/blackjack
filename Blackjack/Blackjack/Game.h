#pragma once
#include "Deck.h"
#include "Player.h"

class Game
{
	const static int playerLimit = 21;
	const static int dealerLimit = 17;
	size_t m_player=0;
	size_t m_dealer=0;
	Deck m_deck;
   Player dealer;
   Player player;

public:
	Game();
	~Game();
	void play();
   void play2();
	bool getPlayerChoice();
};

