#include "Card.h"

Card::Card(Rank currentRank, Suit currentSuit) :
	m_rank(currentRank),
	m_suit(currentSuit)
{

}

Card::Card(const Card& card) :
	m_rank(card.m_rank),
	m_suit(card.m_suit)
{

}

void Card::printCard() const
{
	switch (m_rank)
	{
	case Card::Rank::ACE:
	{
		std::cout << "A ";

		break;
	}
	case Card::Rank::two:
	{
		std::cout << "2 ";

		break;
	}
	case Card::Rank::three:
	{
		std::cout << "3 ";

		break;
	}
	case Card::Rank::four:
	{
		std::cout << "4 ";

		break;
	}
		
	case Card::Rank::five:
	{
		std::cout << "5 ";

		break;
	}
	case Card::Rank::six:
	{
		std::cout << "6 ";

		break;
	}
	case Card::Rank::seven:
	{
		std::cout << "7 ";

		break;
	}
	case Card::Rank::eight:
	{
		std::cout << "8 ";

		break;
	}
	case Card::Rank::nine:
	{
		std::cout << "9 ";

		break;
	}
	case Card::Rank::ten:
	{
		std::cout << "10 ";

		break;
	}
	case Card::Rank::Jack:
	{
		std::cout << "J ";

		break;
	}
	case Card::Rank::Queen:
	{
		std::cout << "Q ";

		break;
	}
	case Card::Rank::King:
	{
		std::cout << "K ";

		break;
	}

	default:
		break;
	}

	switch (m_suit)
	{
	case Card::Suit::Spades:
	{
      std::cout << "S";

		break;
	}
	case Card::Suit::Hearts:
	{
      std::cout << "H";

		break;
	}
	case Card::Suit::Diamonds:
	{
      std::cout << "D";

		break;
	}
	case Card::Suit::Clubs:
	{
      std::cout << "C";

		break;
	}
	default:
		break;
	}
}

int Card::getCardValue() const
{
	int value = static_cast<int>(m_rank);

	if (value == 1)
		return 11;
	else
		if (value >= 2 && value < 10)
			return value;
		else
			return 10;

}

Card& Card::operator= (const Card& other)
{
	m_rank = other.m_rank;
	m_suit = other.m_suit;
	return *this;
}