#pragma once
#include"Card.h"
#include<array>
#include<random>
#include <algorithm> 


class Deck
{
	const size_t m_suitSize = 4;
	const size_t m_rankSize = 13;
	const size_t m_deckSize = 52;
	std::array<Card, 52> m_array;
	size_t m_index=0;

public:
	Deck();
	~Deck();
	void printDeck();
	void shuffleDeck();
	int dealCard();
   Card dealCard2();
};

