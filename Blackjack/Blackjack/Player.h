#pragma once

#include <vector>
#include "Card.h"
class Player
{
protected:
   std::vector<Card> hand;
   size_t score=0;

public:

   void addCard(const Card& card);
   size_t getScore();
   virtual void displayHand();

   Player();
   ~Player();
};

