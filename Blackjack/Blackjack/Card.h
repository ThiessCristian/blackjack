#pragma once
#include <string>
#include <iostream>

class Card
{
public:
	enum class Rank
	{
		null = 0,
		ACE,
		two,
		three,
		four,
		five,
		six,
		seven,
		eight,
		nine,
		ten,
		Jack,
		Queen,
		King

	};

	enum class Suit
	{
		null,
		Spades,
		Hearts,
		Diamonds,
		Clubs
	};

	Card(Rank currentRank = Rank::null, Suit currentSuit = Suit::null);

	Card(const Card& card);

	void printCard() const;

	int getCardValue() const;

	Card& operator= (const Card& other);


private:
	Rank m_rank;
	Suit m_suit;
};

