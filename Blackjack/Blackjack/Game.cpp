#include "Game.h"



Game::Game():m_deck()
{
	m_deck.shuffleDeck();
}


Game::~Game()
{
}

void Game::play2()
{

}

void Game::play()
{
	int cardValue = 0;
	m_dealer = 0;
	m_player = 0;
	int choice = 1;

	cardValue = m_deck.dealCard();

	m_dealer += cardValue;

	std::cout << "[ ]";

	cardValue = m_deck.dealCard();

	std::cout << cardValue;

	m_dealer += cardValue;

	std::cout << std::endl;

	cardValue = m_deck.dealCard();

	std::cout << cardValue << " ";

	m_player += cardValue;

	cardValue = m_deck.dealCard();

	std::cout << cardValue << std::endl;

	m_player += cardValue;

	std::cout << "Your current score: " << m_player << std::endl;

	if (m_player > 21)
	{
		std::cout << "You lose \n";
		return;
	}

	while (choice == 1 || m_player < playerLimit)
	{
		choice = getPlayerChoice();

		if (choice == 1)
		{
			m_player += m_deck.dealCard();
		}
		else if (choice == 0)
		{
			break;
		}
		else continue;
			

		std::cout << "Your current score: " << m_player << std::endl;


		if (m_player > playerLimit)
		{
			std::cout << "You lost!" << std::endl;
			return;
		}
	}

	while (m_dealer <= dealerLimit)
	{
		std::cout << "Your dealer score: " << m_dealer << std::endl;
		system("pause");
		m_dealer += m_deck.dealCard();

		if (m_dealer > playerLimit)
		{
			std::cout << "You win!" << std::endl;
		}
	}

	if (m_player < m_dealer)
	{
		std::cout << "You lose!" << std::endl;
	} 
	else if(m_player > m_dealer)
	{
		std::cout << "You win!" << std::endl;
	}
	else
	{
		std::cout << "It is a draw!" << std::endl;
	}


}

bool Game::getPlayerChoice()
{
   std::cout << "hit(h) or stand(s)" << std::endl;
   bool correctImput = false;
   bool continueGame;
   do {
      char choice;
      std::cin >> choice;
      tolower(choice);
      if (choice == 'h') {
         continueGame = true;
         correctImput = true;
      } else if (choice == 's') {
         continueGame = false;
         correctImput = true;
      } else {
         std::cout << "wrong input, should be h or s" << std::endl;
         correctImput = false;
      }
   } while (!correctImput);
   return continueGame;
}
