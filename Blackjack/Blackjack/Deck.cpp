#include "Deck.h"



Deck::Deck()
{
	int index = 0;
	for (size_t i = 1; i <= m_rankSize; ++i)
	{
		for (size_t j = 1; j <= m_suitSize; ++j)
		{
			m_array[index] = Card(static_cast<Card::Rank>(i), static_cast<Card::Suit>(j));
			++index;
		}
	}
}

Deck::~Deck()
{

}

void Deck::printDeck()
{
	for (auto elem : m_array)
	{
		elem.printCard();
	}
}

void swap(Card& a, Card& b)
{
	Card aux (a);
	a = b;
	b = aux;
}

void Deck::shuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 51);
	for (auto elem : m_array)
	{
		int swapIndex = dis(gen);
		swap(elem, m_array[swapIndex]);
	}
}

int Deck::dealCard()
{
	m_index++;
	return m_array.at(m_index-1).getCardValue();
}

Card Deck::dealCard2()
{
   m_index++;
   return m_array[m_index-1];
}
